// Copyright 2018 Donesky, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package rooteroc

import (
	"cloud.google.com/go/errorreporting"
	"log"
	"context"
	"net/http"
)

type ErrorReporter struct {
	errorClient *errorreporting.Client
}

func NewErrorReporter(ctx context.Context, projectId, serviceName, serviceVersion string) *ErrorReporter {
	var errorClient *errorreporting.Client
	errorClient, err := errorreporting.NewClient(ctx, projectId, errorreporting.Config{
		ServiceName:    serviceName,
		ServiceVersion: serviceVersion,
	})
	if err != nil {
		log.Fatalf("Failed to start error reporting: %s", err.Error())
	}
	return &ErrorReporter{
		errorClient: errorClient,
	}
}

func (e *ErrorReporter) LogError(err error, r *http.Request) {
	e.errorClient.Report(errorreporting.Entry{
		Error: err,
		Req:   r,
	})
}
