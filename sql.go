// Copyright 2018 Donesky, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package rooteroc

import (
	"database/sql"
	"database/sql/driver"

	"github.com/lib/pq"
	"github.com/basvanbeek/ocsql"
)

var drvr driver.Driver

func OpenTracingDB(connstring string) (*sql.DB, error) {
	drvr = ocsql.Wrap(&pq.Driver{}, ocsql.WithAllTraceOptions())
	sql.Register("ocsql-postgres", drvr)
	return sql.Open("ocsql-postgres", connstring)
}
