// Copyright 2018 Donesky, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package rooteroc

import (
	"log"

	"go.opencensus.io/examples/exporter"
	"go.opencensus.io/stats/view"
	"go.opencensus.io/trace"
	"go.opencensus.io/plugin/ochttp"

	"github.com/gorilla/mux"

	"gitlab.com/donesky.io/rooter"
)

func NewConsoleExporterRouter() rooter.RouteManagerRouter {
	console := &exporter.PrintExporter{}
	view.RegisterExporter(console)
	trace.RegisterExporter(console)
	trace.ApplyConfig(trace.Config{DefaultSampler: trace.AlwaysSample()})
	router := mux.NewRouter()
	traceRouter := &ochttp.Handler{
		Handler:          router,
		IsPublicEndpoint: true,
	}
	if err := view.Register(ochttp.DefaultServerViews...); err != nil {
		log.Fatalf("Failed to register default views: %s", err.Error())
	}
	return &opencensusRouter{
		router:      router,
		traceRouter: traceRouter,
	}
}
